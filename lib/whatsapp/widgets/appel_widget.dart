import 'package:flutter/material.dart';
import 'package:formation_flutter_2/helpers/constant.dart';
import 'package:formation_flutter_2/helpers/styles.dart';
import 'package:formation_flutter_2/models/dto.dart';

class AppelWidget extends StatelessWidget {
  const AppelWidget({
    Key? key,
    required this.dto,
  }) : super(key: key);

  final Dto dto;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: const CircleAvatar(
        child: Icon(
          Icons.photo,
        ),
      ),
      onTap: () {},
      title: Text(dto.nom ?? "", style: styleTitle),
      subtitle: Row(
        children: [
          const Icon(
            Icons.north_east,
            size: 16,
            color: appColorGreen2,
          ),
          Text(" (${dto.nbreAppel}) ${dto.dateStatut}"),
        ],
      ),
      trailing: IconButton(
        icon: const Icon(Icons.call),
        onPressed: () {},
      ),
    );
  }
}
