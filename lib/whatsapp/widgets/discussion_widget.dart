import 'package:flutter/material.dart';
import 'package:formation_flutter_2/models/dto.dart';
import 'package:formation_flutter_2/helpers/constant.dart';
import 'package:formation_flutter_2/helpers/styles.dart';

class DiscussionWidget extends StatelessWidget {
  const DiscussionWidget({
    Key? key,
    required this.dto,
  }) : super(key: key);

  final Dto dto;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: const CircleAvatar(
        child: Icon(
          Icons.photo,
        ),
      ),
      title: Text(dto.nom ?? "",
          style: styleTitle,),
      subtitle: const Text(
        defaultLastContent,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
      trailing: const Text("16:30"),
      onTap: () {},
    );
  }
}