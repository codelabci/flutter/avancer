import 'dart:math';

import 'package:flutter/material.dart';
import 'package:formation_flutter_2/helpers/constant.dart';
import 'package:formation_flutter_2/helpers/styles.dart';
import 'package:formation_flutter_2/helpers/utilities.dart';
import 'package:formation_flutter_2/models/dto.dart';
import 'package:formation_flutter_2/whatsapp/widgets/appel_widget.dart';
import 'package:formation_flutter_2/whatsapp/widgets/discussion_widget.dart';
import 'package:grouped_list/grouped_list.dart';

class HomeWhatsapp extends StatefulWidget {
  HomeWhatsapp({Key? key}) : super(key: key);

  @override
  State<HomeWhatsapp> createState() => _HomeWhatsappState();
}

class _HomeWhatsappState extends State<HomeWhatsapp> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: DefaultTabController(
      length: 4,
      initialIndex: 1,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: const Text(appNameWhatsApp),
          actions: [
            IconButton(
                onPressed: () {}, icon: const Icon(Icons.camera_alt_outlined)),
            IconButton(onPressed: () {}, icon: const Icon(Icons.search)),
            PopupMenuButton(itemBuilder: (context) {
              return datasPopupMenuButtonPrincipal
                  .map((data) => PopupMenuItem(
                        child: Row(
                          children: [
                            const Icon(Icons.search, color: Colors.black),
                            Flexible(child: Text(data)),
                          ],
                        ),
                        onTap: () {},
                      ))
                  .toList();
            })
          ],
          bottom: TabBar(
            isScrollable: true,
            indicatorColor: colorWhite,
            indicatorWeight: 3, // pr augmenter la hauteur du indicator
            indicatorSize: TabBarIndicatorSize.tab,
            automaticIndicatorColorAdjustment: true,
            labelPadding: EdgeInsets.zero,
            tabs: [
              SizedBox(
                width: Utilities.getWidthByMediaQuery(context, percent: 0.1),
                child: const Tab(
                  icon: Icon(Icons.add_business),
                ),
              ),
              SizedBox(
                width: Utilities.getWidthByMediaQuery(context, percent: 0.3),
                child: const Tab(
                  text: "DISC.",
                ),
              ),
              SizedBox(
                width: Utilities.getWidthByMediaQuery(context, percent: 0.3),
                child: const Tab(
                  text: "STATUT",
                ),
              ),
              SizedBox(
                width: Utilities.getWidthByMediaQuery(context, percent: 0.3),
                child: const Tab(
                  text: "APPELS",
                ),
              ),
              // WhatsApp()
            ],
          ),
        ),
        body: TabBarView(
          children: [
            ListView.builder(
              itemBuilder: (context, index) {
                var dto = datasUserDiscussion[index];
                // var data = datasUserDiscussion[index].copyWith();
                var map = dto.toMap();
                print("map ::: $map");
                return DiscussionWidget(dto: dto);
              },
              itemCount: datasUserDiscussion.length,
            ),
            ListView.builder(
              itemBuilder: (context, index) {
                var dto = datasUserDiscussion[index];
                return DiscussionWidget(dto: dto);
              },
              itemCount: datasUserDiscussion.length,
            ),
            GroupedListView<dynamic, String>(
              elements: datasUserDiscussion,
              groupBy: (element) => element.groupByAttribut,
              groupHeaderBuilder: (element) {
                var libGroup = "";
                switch (element.groupByAttribut) {
                  case msgMajVue:
                    libGroup = "Mises à jour vues";
                    break;
                  case msgMajSilencieux:
                    libGroup = "Mises à jour en silencieux";

                    break;
                  case msgMajRecent:
                    libGroup = "Mises à jour récentes";

                    break;
                  case msgMonStatut:
                    libGroup = "";

                    break;
                  default:
                }
                return libGroup.isNotEmpty
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(libGroup,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                            )),
                      )
                    : Container();
              },
              itemBuilder: (context, element) {
                return element.groupByAttribut == msgMonStatut
                    ? ListTile(
                        leading: Stack(
                          alignment: AlignmentDirectional.bottomEnd,
                          children: [
                            CircleAvatar(
                              child: Icon(
                                Icons.photo,
                              ),
                            ),
                            Container(
                              color: Colors.red,
                              child: Icon(
                                Icons.add,
                                color: Colors.white,
                                size: 20,
                              ),
                            )
                          ],
                        ),
                        title: const Text(
                          "Mon statut",
                          style: styleTitle,
                        ),
                        subtitle: const Text(
                          "Appuyer pour ajouter un statut",
                        ),
                        onTap: () {},
                      )
                    : ListTile(
                        leading: const CircleAvatar(
                          child: Icon(
                            Icons.photo,
                          ),
                        ),
                        title: Text(
                          element.nom ?? "",
                          style: styleTitle,
                        ),
                        subtitle: Text(
                          element.dateStatut,
                        ),
                        onTap: () {},
                      );
              },
            ),
            ListView.builder(
              itemBuilder: (context, index) {
                dynamic currentWidget;
                switch (index) {
                  case 0:
                    currentWidget = ListTile(
                      onTap: () {},
                      leading: CircleAvatar(
                        // methode 1
                        // child: RotationTransition(
                        //   turns: AlwaysStoppedAnimation(15 / 360),
                        //   child: Icon(Icons.link),
                        // ),
                        child: Transform.rotate(
                          angle: -pi / 4,
                          child: const Icon(Icons.link),
                        ),
                      ),
                      title: const Text("Créer un lien d'appel"),
                      subtitle: const Text(
                        "Partager un lien pour votre appel WhatsApp",
                      ),
                    );
                    break;
                  case 1:
                    currentWidget = const Padding(
                      padding: EdgeInsets.only(left: 16.0),
                      child: Text("Récents",
                          style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.bold,
                          )),
                    );

                    break;

                  default:
                    var dto = datasUserDiscussion[index - 2];

                    currentWidget = AppelWidget(dto: dto);
                }

                return currentWidget;
              },
              itemCount: datasUserDiscussion.length + 2,
            ),
          ],
        ),
      ),
    ));
  }
}
