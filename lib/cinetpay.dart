import 'package:cinetpay/cinetpay.dart';
import 'package:flutter/material.dart';

class TestCinetPay extends StatefulWidget {
  TestCinetPay({Key? key}) : super(key: key);

  @override
  State<TestCinetPay> createState() => _TestCinetPayState();
}

class _TestCinetPayState extends State<TestCinetPay> {
  @override
  Widget build(BuildContext context) {
    return CinetPayCheckout();
  }
}
