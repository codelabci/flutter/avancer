import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:formation_flutter_2/helpers/constant.dart';
import 'package:formation_flutter_2/helpers/styles.dart';
import 'package:formation_flutter_2/helpers/utilities.dart';
import 'package:formation_flutter_2/models/product.dart';
import 'package:mysql1/mysql1.dart';

class HomeEcommerce extends StatefulWidget {
  HomeEcommerce({Key? key}) : super(key: key);

  @override
  State<HomeEcommerce> createState() => _HomeEcommerceState();
}

class _HomeEcommerceState extends State<HomeEcommerce> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initDataBase();
  }

  initDataBase() async {
    print('initDataBase');
    final conn = await MySqlConnection.connect(
      ConnectionSettings(
          host: '192.168.1.6',
          port: 8889,
          user: 'root',
          db: 'close',
          password: 'root'),
    );
    print('conn ::: $conn');

    var results = await conn.query(
      'select * from utilisateur_android',
    );
    for (var row in results) {
      print('Rows : $row');
    }
    // Finally, close the connection
    await conn.close();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                const ListTile(
                  contentPadding: EdgeInsets.zero,
                  title: Text(
                    "Hey Nency,",
                    style: styleTitle,
                  ),
                  subtitle: Text("Begin your shopping !!"),
                  trailing: Icon(Icons.notification_important_outlined),
                ),
                Container(
                  // padding: EdgeInsets.all(8.0),
                  decoration: BoxDecoration(color: greenFondEcommerce),
                  height:
                      Utilities.getHeightByMediaQuery(context, percent: 0.2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Enjoy Upto \n50% Discount"),
                          ElevatedButton(
                            onPressed: () {},
                            child: Text("Subscribe"),
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Image.asset(
                          "images/1.jpeg",
                        ),
                      )
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Top Categories',
                      style: styleBloc,
                    ),
                    TextButton(child: Text('SEE ALL'), onPressed: () {}),
                  ],
                ),
                SizedBox(
                  height:
                      Utilities.getHeightByMediaQuery(context, percent: 0.1),
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return Container(
                        padding: EdgeInsets.all(8),
                        margin: EdgeInsets.all(4),
                        decoration: BoxDecoration(
                            color: Colors.grey.shade300,
                            borderRadius: BorderRadius.circular(4)),
                        child: Icon(Icons.settings),
                      );
                    },
                    itemCount: 10,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'New Arrivals',
                      style: styleBloc,
                    ),
                    TextButton(child: Text('VIEW MORE'), onPressed: () {}),
                  ],
                ),
                ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    var currentProduct = datasProduct[index];
                    return OpenContainer(
                        // openElevation: 0.0,
                        transitionDuration: const Duration(milliseconds: 500),
                        tappable: true,
                        transitionType: ContainerTransitionType.fadeThrough,
                        closedBuilder: (context, action) {
                          // action.call();

                          return ProductWidget(currentProduct: currentProduct);
                        },
                        openBuilder: (context, action) {
                          return Scaffold(
                            appBar: AppBar(),
                            body: Center(
                              child: Text("animations"),
                            ),
                          );
                        });
                  },
                  itemCount: datasProduct.length,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ProductWidget extends StatelessWidget {
  const ProductWidget({
    Key? key,
    required this.currentProduct,
  }) : super(key: key);

  final Product currentProduct;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(4),
      decoration: BoxDecoration(
          color: Colors.grey.shade300, borderRadius: BorderRadius.circular(4)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
              height: Utilities.getHeightByMediaQuery(context, percent: 0.2),
              width: Utilities.getWidthByMediaQuery(context, percent: 0.3),
              child: Image.asset(
                currentProduct.image ?? "",
                // fit: BoxFit.contain,
              )),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                currentProduct.name ?? "",
                style: styleTitleProduct,
              ),
              Text(
                currentProduct.price ?? "",
                style: styleBloc,
              ),
              ElevatedButton(
                  onPressed: () {},
                  child: Row(
                    children: [
                      Icon(Icons.star_border),
                      Text(currentProduct.note ?? "")
                    ],
                  ))
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                onPressed: () {},
                icon: Icon(currentProduct.isFavorite
                    ? Icons.favorite
                    : Icons.favorite_border),
              ),
              IconButton(
                onPressed: () {},
                icon: Icon(Icons.add),
              ),
            ],
          )
        ],
      ),
    );
  }
}
