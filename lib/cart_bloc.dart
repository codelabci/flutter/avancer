import 'package:bloc/bloc.dart';

//event
abstract class CartEvent {}

class CartIncrementEvent extends CartEvent {}

class CartDecrementEvent extends CartEvent {}

//state
abstract class CartState {
  int count;
  CartState({
    this.count = 0,
  });
}

class CartSuccesState extends CartState {
  CartSuccesState({int count = 0}) : super(count: count);
}

class CartErrorState extends CartState {
  String? message;
  CartErrorState({this.message, int count = 0}) : super(count: count);
}

class CartLodingState extends CartState {
  CartLodingState({int count = 0}) : super(count: count);
}

class CartInitialState extends CartState {
  CartInitialState() : super();
}

class CartBloc extends Bloc<CartEvent, CartState> {
  // CartBloc(super.initialState);

  CartBloc() : super(CartInitialState()) {
    on((CartIncrementEvent event, emit) {
      print("ezrf :: ${state.count}");

      print("CartIncrementEvent");
      // emit loading event
      emit(CartLodingState(count: state.count));
      print("ezrf after loading:: ${state.count}");

      // traitement
      state.count = state.count + 1;

      // emit last event
      // emit(state);
      print("ezrf after :: ${state.count}");

      emit(CartSuccesState(count: state.count));
    });

    on((CartDecrementEvent event, emit) {
      print("CartDecrementEvent");

      // emit loading event
      emit(CartLodingState(count: state.count));

      // traitement & emit last event
      if (state.count == 0) {
        emit(
          CartErrorState(
              message: "Impossible de décrémenter une valeur nulle !!!",
              count: state.count),
        );
      } else {
        state.count--;
        emit(CartSuccesState(count: state.count));
      }

      // state.count--;
      // emit(CartSuccesState(count: state.count));

      // emit last event
      // emit(CartSuccesState());
    });
  }
}
