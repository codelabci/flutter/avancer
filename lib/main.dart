import 'package:cinetpay/cinetpay.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formation_flutter_2/bloc_widget.dart';
import 'package:formation_flutter_2/cart_bloc.dart';
import 'package:formation_flutter_2/e-commerce/home_ecommerce.dart';
import 'package:formation_flutter_2/helpers/constant.dart';
import 'package:formation_flutter_2/map_test.dart';
import 'package:formation_flutter_2/whatsapp/home_whatsapp.dart';

void main() {
  runApp(MultiBlocProvider(
    providers: [
      BlocProvider(create: (context) => CartBloc()),
    ],
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Formation Niv 2',
      theme: ThemeData(
        useMaterial3: true,
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        // primarySwatch: Colors.,

        // primarySwatch: Colors.white,
        // colorScheme:  ,

        colorSchemeSeed: Colors.green,

        // bottomAppBarColor: appMaterialColorGreen1
      ),
      home: BlocWidgetTest(),
      // home: MapTest(),
      // home: CinetPayCheckout(
      //   title: 'YOUR_TITLE',

      // ),
    );
  }
}
