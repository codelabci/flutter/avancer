import 'package:flutter/material.dart';

class Utilities {
  static getWidthByMediaQuery(BuildContext context, {double percent = 1}) {
    return MediaQuery.of(context).size.width * percent;
  }

  static getHeightByMediaQuery(BuildContext context, {double percent = 1}) {
    return MediaQuery.of(context).size.height * percent;
  }
}
