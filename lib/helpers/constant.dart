import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:formation_flutter_2/models/dto.dart';
import 'package:formation_flutter_2/models/message.dart';
import 'package:formation_flutter_2/models/product.dart';

const appNameWhatsApp = "Whatsapp Business";

// colors

const appColorGreen1 = Color(0xFF075E54);
const appColorGreen2 = Color(0xFF128C7E);
const appColorGreen3 = Color(0XFF25D366);
const appColorGreen4 = Color(0XFFDCF8C6);
const appColorBlue = Color(0XFF34B7F1);
const appColorWhite = Color(0XFFECE5DD);
const colorWhite = Colors.white;

final greenFondEcommerce = appColorGreen3.withOpacity(0.3);
const greenButtonEcommerce = appColorGreen2;
final greenTextEcommerce = appColorGreen2.withOpacity(0.6);

const Map<int, Color> colorGreen1 = {
  50: Color.fromRGBO(7, 94, 84, .1),
  100: Color.fromRGBO(7, 94, 84, .2),
  200: Color.fromRGBO(7, 94, 84, .3),
  300: Color.fromRGBO(7, 94, 84, .4),
  400: Color.fromRGBO(7, 94, 84, .5),
  500: Color.fromRGBO(7, 94, 84, .6),
  600: Color.fromRGBO(7, 94, 84, .7),
  700: Color.fromRGBO(7, 94, 84, .8),
  800: Color.fromRGBO(7, 94, 84, .9),
  900: Color.fromRGBO(7, 94, 84, 1),
};

const MaterialColor appMaterialColorGreen1 =
    MaterialColor(0xFF075E54, colorGreen1);

final datasProduct = [
  Product(
      name: "Pink & Blue Top",
      price: "\$150.0",
      note: "4.5",
      isFavorite: true,
      image: "images/1.jpeg"),
  Product(
      name: "White off shoulder",
      price: "\$135.5",
      note: "3.5",
      image: "images/2.jpeg"),
  Product(
      name: "Red & Blue Jacket",
      price: "\$170.0",
      note: "4.0",
      isFavorite: true,
      image: "images/3.jpeg"),
  Product(
      name: "Trendy White Top",
      price: "\$129.9",
      note: "3.0",
      isFavorite: true,
      image: "images/4.jpeg"),
];

// datas
const datasPopupMenuButtonPrincipal = [
  "Faire de la publicité sur Facebook",
  "Outils professionels",
  "Nouveau groupe",
  "Nouvelle diffusion",
  "Etiquettes",
  "Appareils connectés",
  "Messages importants",
  "Paramètres",
];

// final datasCategories = [Icons.al];

final datasUserDiscussion = [
  Dto(
      nom: "Diallo zie",
      datasMessage: datasMessage,
      groupByAttribut: msgMonStatut),
  Dto(nom: "Coulibaly zana", datasMessage: datasMessage),
  Dto(nom: "Toure thierno", datasMessage: datasMessage),
  Dto(nom: "Kone seriba", datasMessage: datasMessage),
  Dto(nom: "Famille Kone", isGroup: true, datasMessage: datasMessage),
  Dto(nom: "Traoré ", datasMessage: datasMessage),
  Dto(
      nom: "Kaba zie",
      datasMessage: datasMessage,
      groupByAttribut: msgMajSilencieux),
  Dto(nom: "Famille Coulibaly", isGroup: true, datasMessage: datasMessage),
  Dto(nom: "Famille Diallo", isGroup: true, datasMessage: datasMessage),
  Dto(
      nom: "Famille toure",
      isGroup: true,
      datasMessage: datasMessage,
      groupByAttribut: msgMajSilencieux),
  Dto(nom: "Sanogo zie", datasMessage: datasMessage),
  Dto(
      nom: "Tall zie",
      datasMessage: datasMessage,
      groupByAttribut: msgMajSilencieux),
  Dto(
      nom: "Sekongo n'golo",
      datasMessage: datasMessage,
      groupByAttribut: msgMajSilencieux),
  Dto(nom: "Bah", datasMessage: datasMessage),
  Dto(
      nom: "Tahirou zie",
      datasMessage: datasMessage,
      groupByAttribut: msgMajVue),
  Dto(nom: "Astou zie", datasMessage: datasMessage, groupByAttribut: msgMajVue),
  Dto(nom: "Zeinab", datasMessage: datasMessage),
  Dto(
      nom: "Grande soeur",
      datasMessage: datasMessage,
      groupByAttribut: msgMajVue),
  Dto(nom: "Zie", datasMessage: datasMessage),
  Dto(
      nom: "Diallo zie",
      datasMessage: datasMessage,
      groupByAttribut: msgMajVue),
  Dto(
      nom: "Diallo zie",
      datasMessage: datasMessage,
      groupByAttribut: msgMajVue),
];

const defaultDate = "Aujourd'hui à 15:09";
const defaultLastContent =
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry.";

const msgMajRecent = "B";
const msgMajVue = "C";
const msgMonStatut = "A";
const msgMajSilencieux = "D";
final datasMessage = [
  Message(contenu: defaultLastContent),
  Message(contenu: defaultLastContent),
  Message(contenu: defaultLastContent),
  Message(contenu: defaultLastContent),
  Message(contenu: defaultLastContent, isMe: true),
  Message(contenu: defaultLastContent, isMe: true),
  Message(contenu: defaultLastContent),
  Message(contenu: defaultLastContent),
  Message(contenu: defaultLastContent, isMe: true),
  Message(contenu: defaultLastContent),
  Message(contenu: defaultLastContent, isMe: true),
  Message(contenu: defaultLastContent, isMe: true),
];
