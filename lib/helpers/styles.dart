import 'package:flutter/material.dart';
import 'package:formation_flutter_2/helpers/constant.dart';

const styleTitle = TextStyle(fontWeight: FontWeight.w600, fontSize: 20);
const styleBloc = TextStyle(fontWeight: FontWeight.w500, fontSize: 16);
const styleTitleProduct = TextStyle(fontWeight: FontWeight.w100);
const styleSeeAll =
    TextStyle(fontWeight: FontWeight.w500, color: appColorGreen1);
