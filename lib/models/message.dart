import 'dart:convert';

class Message {
  int? id;
  String? contenu;
  String? media;
  bool? isMe;
  bool? isLike;
  bool? isOk;
  Message({
    this.id,
    this.contenu,
    this.media,
    this.isMe = false,
    this.isLike,
    this.isOk,
  });
  Message? parent;

  Message copyWith({
    int? id,
    String? contenu,
    String? media,
    bool? isMe,
    bool? isLike,
    bool? isOk,
  }) {
    return Message(
      id: id ?? this.id,
      contenu: contenu ?? this.contenu,
      media: media ?? this.media,
      isMe: isMe ?? this.isMe,
      isLike: isLike ?? this.isLike,
      isOk: isOk ?? this.isOk,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'contenu': contenu,
      'media': media,
      'isMe': isMe,
      'isLike': isLike,
      'isOk': isOk,
    };
  }

  factory Message.fromMap(Map<String, dynamic> map) {
    return Message(
      id: map['id']?.toInt(),
      contenu: map['contenu'],
      media: map['media'],
      isMe: map['isMe'],
      isLike: map['isLike'],
      isOk: map['isOk'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Message.fromJson(String source) =>
      Message.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Message(id: $id, contenu: $contenu, media: $media, isMe: $isMe, isLike: $isLike, isOk: $isOk)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Message &&
        other.id == id &&
        other.contenu == contenu &&
        other.media == media &&
        other.isMe == isMe &&
        other.isLike == isLike &&
        other.isOk == isOk;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        contenu.hashCode ^
        media.hashCode ^
        isMe.hashCode ^
        isLike.hashCode ^
        isOk.hashCode;
  }
}
