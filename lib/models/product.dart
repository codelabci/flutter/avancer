import 'dart:convert';

class Product {
  String? name;
  String? image;
  String? price;
  String? note;
  bool isFavorite;

  Product({
    this.name,
    this.image,
    this.price,
    this.note,
    this.isFavorite = false,
  });

  Product copyWith({
    String? name,
    String? image,
    String? price,
    String? note,
    bool? isFavorite,
  }) {
    return Product(
      name: name ?? this.name,
      image: image ?? this.image,
      price: price ?? this.price,
      note: note ?? this.note,
      isFavorite: isFavorite ?? this.isFavorite,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'image': image,
      'price': price,
      'note': note,
      'isFavorite': isFavorite,
    };
  }

  factory Product.fromMap(Map<String, dynamic> map) {
    return Product(
      name: map['name'],
      image: map['image'],
      price: map['price'],
      note: map['note'],
      isFavorite: map['isFavorite'] ?? false,
    );
  }

  String toJson() => json.encode(toMap());

  factory Product.fromJson(String source) =>
      Product.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Product(name: $name, image: $image, price: $price, note: $note, isFavorite: $isFavorite)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Product &&
        other.name == name &&
        other.image == image &&
        other.price == price &&
        other.note == note &&
        other.isFavorite == isFavorite;
  }

  @override
  int get hashCode {
    return name.hashCode ^
        image.hashCode ^
        price.hashCode ^
        note.hashCode ^
        isFavorite.hashCode;
  }
}
