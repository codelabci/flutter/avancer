import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:formation_flutter_2/helpers/constant.dart';
import 'package:formation_flutter_2/models/message.dart';

class Dto {
  int? id;
  String? nom;
  String? prenoms;
  String? profil;
  String? telephone;
  String? derniereConnexion;
  bool? isGroup;
  bool? isVue;
  bool? isSilence;
  String dateStatut;
  String dateAppel;
  String groupByAttribut;
  int nbreAppel;
  List<Message>? datasMessage;

  Dto({
    this.id,
    this.nom,
    this.prenoms,
    this.profil,
    this.telephone,
    this.derniereConnexion,
    this.isGroup = false,
    this.isVue,
    this.isSilence,
    this.dateStatut = defaultDate,
    this.dateAppel = defaultDate,
    this.nbreAppel = 2,
    this.groupByAttribut = msgMajRecent,
    this.datasMessage,
  });

  Dto copyWith({
    int? id,
    String? nom,
    String? prenoms,
    String? profil,
    String? telephone,
    String? derniereConnexion,
    bool? isGroup,
    bool? isVue,
    bool? isSilence,
    String? dateStatut,
    String? dateAppel,
    String? groupByAttribut,
    int? nbreAppel,
    List<Message>? datasMessage,
  }) {
    return Dto(
      id: id ?? this.id,
      nom: nom ?? this.nom,
      prenoms: prenoms ?? this.prenoms,
      profil: profil ?? this.profil,
      telephone: telephone ?? this.telephone,
      derniereConnexion: derniereConnexion ?? this.derniereConnexion,
      isGroup: isGroup ?? this.isGroup,
      isVue: isVue ?? this.isVue,
      isSilence: isSilence ?? this.isSilence,
      dateStatut: dateStatut ?? this.dateStatut,
      dateAppel: dateAppel ?? this.dateAppel,
      groupByAttribut: groupByAttribut ?? this.groupByAttribut,
      nbreAppel: nbreAppel ?? this.nbreAppel,
      datasMessage: datasMessage ?? this.datasMessage,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'nom': nom,
      'prenoms': prenoms,
      'profil': profil,
      'telephone': telephone,
      'derniereConnexion': derniereConnexion,
      'isGroup': isGroup,
      'isVue': isVue,
      'isSilence': isSilence,
      'dateStatut': dateStatut,
      'dateAppel': dateAppel,
      'groupByAttribut': groupByAttribut,
      'nbreAppel': nbreAppel,
      'datasMessage': datasMessage?.map((x) => x.toMap()).toList(),
    };
  }

  factory Dto.fromMap(Map<String, dynamic> map) {
    return Dto(
      id: map['id']?.toInt(),
      nom: map['nom'],
      prenoms: map['prenoms'],
      profil: map['profil'],
      telephone: map['telephone'],
      derniereConnexion: map['derniereConnexion'],
      isGroup: map['isGroup'],
      isVue: map['isVue'],
      isSilence: map['isSilence'],
      dateStatut: map['dateStatut'] ?? '',
      dateAppel: map['dateAppel'] ?? '',
      groupByAttribut: map['groupByAttribut'] ?? '',
      nbreAppel: map['nbreAppel']?.toInt() ?? 0,
      datasMessage: map['datasMessage'] != null
          ? List<Message>.from(
              map['datasMessage']?.map((x) => Message.fromMap(x)))
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Dto.fromJson(String source) => Dto.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Dto(id: $id, nom: $nom, prenoms: $prenoms, profil: $profil, telephone: $telephone, derniereConnexion: $derniereConnexion, isGroup: $isGroup, isVue: $isVue, isSilence: $isSilence, dateStatut: $dateStatut, dateAppel: $dateAppel, groupByAttribut: $groupByAttribut, nbreAppel: $nbreAppel, datasMessage: $datasMessage)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Dto &&
        other.id == id &&
        other.nom == nom &&
        other.prenoms == prenoms &&
        other.profil == profil &&
        other.telephone == telephone &&
        other.derniereConnexion == derniereConnexion &&
        other.isGroup == isGroup &&
        other.isVue == isVue &&
        other.isSilence == isSilence &&
        other.dateStatut == dateStatut &&
        other.dateAppel == dateAppel &&
        other.groupByAttribut == groupByAttribut &&
        other.nbreAppel == nbreAppel &&
        listEquals(other.datasMessage, datasMessage);
  }

  @override
  int get hashCode {
    return id.hashCode ^
        nom.hashCode ^
        prenoms.hashCode ^
        profil.hashCode ^
        telephone.hashCode ^
        derniereConnexion.hashCode ^
        isGroup.hashCode ^
        isVue.hashCode ^
        isSilence.hashCode ^
        dateStatut.hashCode ^
        dateAppel.hashCode ^
        groupByAttribut.hashCode ^
        nbreAppel.hashCode ^
        datasMessage.hashCode;
  }
}
