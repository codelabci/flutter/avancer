import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formation_flutter_2/cart_bloc.dart';

class BlocWidgetTest extends StatelessWidget {
  const BlocWidgetTest({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("build BlocWidgetTest");
    return Scaffold(
      body: Center(
        child: Stack(
          alignment: Alignment.center,
          children: [
            IconButton(
              icon: const Icon(
                Icons.shopping_cart_outlined,
                // size: 35,
                // color: Theme.of(context).primaryColor,
              ),
              onPressed: () {},
            ),
            Positioned(
              right: 8,
              top: 8,
              child: Container(
                padding: const EdgeInsets.all(2.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.deepOrangeAccent),
                constraints: const BoxConstraints(
                  minWidth: 16,
                  minHeight: 16,
                ),
                child:
                    BlocBuilder<CartBloc, CartState>(builder: (context, state) {
                  print("build BlocBuilder");

                  if (state is CartLodingState) {
                    return const CircularProgressIndicator();
                  } else {
                    if (state is CartSuccesState) {
                      return Text(
                        '${state.count}',
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontSize: 10,
                        ),
                      );
                    } else {
                      if (state is CartInitialState) {
                        return Text(
                          '${state.count}',
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            fontSize: 10,
                          ),
                        );
                      } else if (state is CartErrorState) {
                        print("build BlocBuilder state ${state.message}");

                        return const Text(
                          'er',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 10,
                          ),
                        );
                      } else {
                        return Container();
                      }
                    }
                  }
                }),
              ),
            )
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FloatingActionButton(
            onPressed: () {
              // envoi de event
              context.read<CartBloc>().add(CartDecrementEvent());
            },
            backgroundColor: Colors.white,
            child: const Icon(Icons.remove),
          ),
          FloatingActionButton(
            onPressed: () {
              // envoi de event
              context.read<CartBloc>().add(CartIncrementEvent());
            },
            backgroundColor: Colors.white,
            child: const Icon(Icons.add),
          ),
        ],
      ),
    );
  }
}
